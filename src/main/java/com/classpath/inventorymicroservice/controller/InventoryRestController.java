package com.classpath.inventorymicroservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
@Slf4j
public class InventoryRestController {

    private static int counter  = 1000;
    @GetMapping
    public Integer counter(){
        log.info("Inside the counter method of InventoryService :: ");
        return counter;
    }
    @PostMapping
    public Integer updated(){
        log.info("Inside the update counter method of InventoryService :: ");
        return --counter;
    }
}
