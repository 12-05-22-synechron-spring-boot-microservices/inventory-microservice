package com.classpath.inventorymicroservice.event;


import com.classpath.inventorymicroservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class OrderEvent {
    private Order order;
    private OrderStatus orderStatus;
    private LocalDateTime orderTime;
}
