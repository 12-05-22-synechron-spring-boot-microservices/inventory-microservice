package com.classpath.inventorymicroservice.event;

public enum OrderStatus {
    ORDER_ACCEPTED,
    ORDER_REJECTED,
    ORDER_CANCELLED
}
